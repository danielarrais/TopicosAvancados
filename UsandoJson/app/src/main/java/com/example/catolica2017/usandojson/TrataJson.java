package com.example.catolica2017.usandojson;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import java.util.ArrayList;

/**
 * Created by catolica2017 on 26/10/17.
 */

public class TrataJson {
    public ArrayList<Carro> carroArrayList(String json) throws JSONException {
        return new Gson().fromJson(json, new TypeToken<ArrayList<Carro>>(){}.getType());
    }
}
