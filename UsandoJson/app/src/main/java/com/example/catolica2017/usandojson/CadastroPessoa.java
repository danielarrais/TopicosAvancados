package com.example.catolica2017.usandojson;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class CadastroPessoa extends AppCompatActivity {

    EditText etNome;
    EditText etSobreNome;
    EditText etNasc;
    TextView lbNome;
    TextView lbSobreNome;
    TextView lbNasc;

    TratarTXT tratarTXT = new TratarTXT(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_pessoa);

        etNome = (EditText) findViewById(R.id.etNome);
        etSobreNome = (EditText) findViewById(R.id.etSobrenome);
        etNasc = (EditText) findViewById(R.id.etNasc);
        lbNome = (TextView) findViewById(R.id.lbNome);
        lbSobreNome = (TextView) findViewById(R.id.lbSob);
        lbNasc = (TextView) findViewById(R.id.lbNasc);
    }

    public void salvar(View view) throws IOException {
        String dados = etNome.getText().toString()+";"+etSobreNome.getText().toString()+";"+etNasc.getText().toString()+";";
        tratarTXT.gravarTXT(dados);
        tratarTXT.imprimirNoLog();
    }
    public void telaCadastros(View view){
        Intent verCadastros = new Intent(this, VisualizarDados.class);
        startActivity(verCadastros);
    }

    public void limpar(View view) throws IOException {
        tratarTXT.apagarDados();
        tratarTXT.imprimirNoLog();
    }

}
