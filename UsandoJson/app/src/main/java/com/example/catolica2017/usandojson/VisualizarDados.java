package com.example.catolica2017.usandojson;

import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.BaseAdapter;
import android.widget.ListView;

import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class VisualizarDados extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_dados);
        BaixarJson baixarJson = new BaixarJson();

        //Caso outra versão, pedir permissão explicita para acesso a internet:
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.INTERNET},12);

        ListView listView = (ListView) findViewById(R.id.lvPessoas);

        BaseAdapter baseAdapter = null;
        try {
            baseAdapter = new AdaptadorPessoa(baixarJson.execute(new URL("http://www.livroandroid.com.br/livro/carros/v2/carros_classicos.json")).get(), this);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        listView.setAdapter(baseAdapter);

    }
}
