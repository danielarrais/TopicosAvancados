package com.example.catolica2017.androidbd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by catolica2017 on 16/11/17.
 */

public class ClassBD extends SQLiteOpenHelper {

    String[] scripCriaBanco = {"create table carro (id integer primary key autoincrement, nome text not null, placa text not null, ano text not null);}"};
    public final String apagaBancoSQL = "drop database if exists carro";
    Context context;
    public ClassBD(Context context, String name, int version) {
        super(context, name, null, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < scripCriaBanco.length;i++){
            db.execSQL(scripCriaBanco[i]);
        }
    }

    public void insereCarro(ContentValues values){
        SQLiteDatabase database = this.getWritableDatabase();
        database.insert("carro", null, values);
        Toast.makeText(context, "Carro inserido", Toast.LENGTH_SHORT).show();
    }

    public void removeCarro(String placa){
        SQLiteDatabase database = this.getWritableDatabase();
        int deletou = database.delete("carro", "placa = \""+placa+"\"", null);
        if(deletou>0){
            Toast.makeText(context, "Carro com placa "+placa+" removido", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context, "Carro com placa "+placa+" não removido", Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<Carro> listaCarro(){

        ArrayList<Carro> carros = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query("carro", new String[]{"id","nome", "placa", "ano"}, null, null, null, null, null);

        while(cursor.moveToNext()){
            carros.add(new Carro(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3)));
        }

        return carros;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
