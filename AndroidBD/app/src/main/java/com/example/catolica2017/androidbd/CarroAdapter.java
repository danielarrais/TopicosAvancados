package com.example.catolica2017.androidbd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;

public class CarroAdapter extends BaseAdapter {

    public ArrayList<Carro> carros;
    private Context context;

    public CarroAdapter(ArrayList<Carro> pessoas, Context contexto) {
        this.carros = pessoas;
        this.context = contexto;
    }

    @Override
    public int getCount() {
        return carros.size();
    }

    @Override
    public Object getItem(int position) {
        return carros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View reciclado, ViewGroup parent) {
        View layout = null;
        if (reciclado==null){
            layout = LayoutInflater.from(context).inflate(R.layout.activity_celula, parent, false);
        }else{
            layout = reciclado;
        }

        final Carro carro = (Carro) getItem(position);

        TextView nome = layout.findViewById(R.id.lbNome);
        TextView placa = layout.findViewById(R.id.lbPlaca);
        TextView ano = layout.findViewById(R.id.lbAno);

        Button excluir = layout.findViewById(R.id.bExcluir);
        excluir.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                ClassBD classBD = new ClassBD(context, "carro", 1);
                classBD.removeCarro(carro.getPlaca());
            }
        });

        nome.setText(carro.getNome());
        placa.setText(carro.getPlaca());
        ano.setText(carro.getAno());

        return layout;
    }

    public void remover(Carro carro){
        ClassBD classBD = new ClassBD(context, "carro", 1);
        classBD.removeCarro(carro.getPlaca());
    }
}