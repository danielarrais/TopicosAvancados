package com.example.catolica2017.androidbd;

/**
 * Created by catolica2017 on 16/11/17.
 */

public class Carro {
    Integer id;
    String nome;
    String placa;
    String ano;

    public Carro() {
    }

    public Carro(String nome, String placa, String ano) {
        this.nome = nome;
        this.placa = placa;
        this.ano = ano;
    }

    public Carro(Integer id, String nome, String placa, String ano) {
        this.id = id;
        this.nome = nome;
        this.placa = placa;
        this.ano = ano;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }
}
