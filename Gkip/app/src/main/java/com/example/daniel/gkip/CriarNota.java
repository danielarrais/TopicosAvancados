package com.example.daniel.gkip;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class CriarNota extends AppCompatActivity {
    EditText etNota;
    EditText etTitulo;
    TextView tvEditar;
    String id;
    Nota nota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_nota);

        //Intancia os TextViews da atividade
        etNota = findViewById(R.id.etNota);
        etTitulo = findViewById(R.id.etTitulo);
        tvEditar = findViewById(R.id.tvEditar);

        //Verifica se tem informações extras na intent
        if (getIntent().getExtras()!=null) {
            //Verifica se foi passado algum id de alguma nota, via intenção
            id = getIntent().getExtras().getString("id");
            //Se foi, isso significa que a nota vai ser editada
            if (id != null) {
                tvEditar.setText("Editar nota");
                ClassBD classBD = new ClassBD(this, "notas", 1);
                nota = classBD.buscarNota(Integer.valueOf(id));
                etNota.setText(nota.getNota());
                etTitulo.setText(nota.getTitulo());
            }
        }
    }

    public void salvarNota(View view) {
        //Recupera o conteúdo inseridos pelo usuário
        String tituloET = etTitulo.getText().toString();
        String notaEt = etNota.getText().toString();
        //Verifica se os campos são vazios, e preenche com um valor padrão
        if (tituloET.equalsIgnoreCase("")) {
            tituloET = "Nota sem nome";
        }
        if (notaEt.equalsIgnoreCase("")) {
            notaEt = "Nota sem conteúdo";
        }
        //Cria uma instância de um content values, onde serão armazenadas as informações da nota
        //para posterior salvamento no banco
        ContentValues contentValues = new ContentValues();
        contentValues.put("nota", etNota.getText().toString());
        contentValues.put("titulo", tituloET);
        //Intância a class que manipula o banco
        ClassBD classBD = new ClassBD(getBaseContext(), "notas", 1);

        //verifica se existe uma nota carregada do banco
        //se não, significa que uma nova nota vai ser cadastrada
        if (nota== null) {
            //Uma data é adicionada ao content values
            contentValues.put("data", String.valueOf(System.currentTimeMillis()));
            //A nota é inserida, e o id da nova nota guardado
            long id = classBD.insereNota(contentValues);
            //Um intent é criada, e ela adicionada o id da nova nota
            Intent intent = new Intent(this, VerNota.class);
            Bundle b = new Bundle();
            b.putString("id", String.valueOf(id));
            intent.putExtras(b);
            //A intent é estartada, e o usuário verá a tela da nova nota
            startActivity(intent);
        }else{
            //Se a nota foi apenas editada, o método update é chamado
            classBD.updateNota(contentValues, nota.getId());
        }
        //a tela de edição é finalizada
        super.finish();
    }

    //método que leva o usuário de volta ao início, caso ele descarte a nota
    public void home(View view) {
        //se ele estava editanto, nada acontece além da finalização da atividade
        if (nota == null) {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
        }
        super.finish();
    }
}
