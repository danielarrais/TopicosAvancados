package com.example.daniel.gkip;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class Notas extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);

        //Recuperando listView
        listView = (ListView) findViewById(R.id.lvNotas);

        //Criando instância da classe manipuladora do banco
        ClassBD classBD = new ClassBD(getBaseContext(), "notas", 1);

        //Criando intância do adapter, passando o retorno do método que retorna a
        //lista de notas cadastradas no banco
        BaseAdapter baseAdapter = new NotaAdapter(classBD.listaNotas(), this);

        //Criação da classe que executará ações de clique dos itens da listView
        AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //O usuário ao clicar é enviado para tela onde visualizará a nota
                Intent intent = new Intent(Notas.this, VerNota.class);
                Bundle b = new Bundle();
                b.putString("id", ((Nota) adapterView.getItemAtPosition(i)).getId() + "");
                intent.putExtras(b);
                startActivity(intent);
            }
        };
        listView.setOnItemClickListener(onItemClickListener);

        //setando o adapter ao listView
        listView.setAdapter(baseAdapter);
    }

    @Override
    protected void onResume() {
        //Criando instância da classe manipuladora do banco
        ClassBD classBD = new ClassBD(getBaseContext(), "notas", 1);

        //Criando intância do adapter, passando o retorno do método que retorna a
        //lista de notas cadastradas no banco
        BaseAdapter baseAdapter = new NotaAdapter(classBD.listaNotas(), this);
        listView.setAdapter(baseAdapter);
        super.onResume();
    }
}
