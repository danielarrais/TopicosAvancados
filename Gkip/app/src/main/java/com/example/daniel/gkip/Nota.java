package com.example.daniel.gkip;

import java.util.Date;

/**
 * Created by daniel on 02/12/17.
 */

public class Nota {
    private int id;
    public String titulo;
    public String nota;
    public String date;
    boolean arquivado;


    public Nota() {
    }

    public Nota(int id, String titulo, String nota, String date, Boolean arquivado) {
        this.id = id;
        this.titulo = titulo;
        this.nota = nota;
        this.date = date;
        this.arquivado = arquivado;
    }

    public boolean isArquivado() {
        return arquivado;
    }

    public void setArquivado(boolean arquivado) {
        this.arquivado = arquivado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
