package com.example.daniel.gkip;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    //Método que encaminha o usuário para tela de inserção de uma nota
    public void telaCadastro(View view){
        Intent verCadastros = new Intent(this, CriarNota.class);
        startActivity(verCadastros);
    }

    //Método que encaminha o usuário para tela de visualização das notas
    public void telaVizualizaNotas(View view){
        Intent verCadastros = new Intent(this, Notas.class);
        startActivity(verCadastros);
    }

    //Método que encaminha o usuário para tela de visualização das notas arquivadas
    public void telaVizualizaArquivo(View view){
        Intent verCadastros = new Intent(this, Arquivo.class);
        startActivity(verCadastros);
    }

    //Método que encaminha o usuário para tela sobre o aplicativo
    public void telaSobre(View view){
        Intent verCadastros = new Intent(this, Sobre.class);
        startActivity(verCadastros);
    }

    //Método que encerra o aplicativo
    public void sair(View view){
        finishAndRemoveTask ();
    }
}
