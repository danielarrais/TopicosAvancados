package com.example.catolica2017.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
    Map<String, String> map = new HashMap<>();

    EditText etEmail = null;
    EditText etSenha = null;

    //Objeto para pessistir dados simples
    SharedPreferences lgShared;

    final int COD_SEGUNDA_TELA = 1;
    TextView label ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = (EditText) findViewById(R.id.email);
        etSenha = (EditText) findViewById(R.id.senha);

        map.put("danielarraiscarvalho@gmail.com","123456789");
        map.put("daniel@gmail.com","123456789");
        map.put("gustavo@gmail.com","123456789");
        lgShared = getSharedPreferences("login", MODE_PRIVATE);
        map.put("a@a.a","a");
        label = (TextView) findViewById(R.id.label) ;
        String senhab = lgShared.getString("senha","Erro!");
        label.setText(senhab);

    }


    public void verificaLogin(){
        String email = etEmail.getText().toString();
        String senha = etSenha.getText().toString();
        if (map.containsKey(email)){
            if (map.get(email).equals(senha)){
                Intent enviaIMC = new Intent(this, LoginSucesso.class);
                Bundle b = new Bundle();
                b.putString("email", email);
                b.putString("senha", senha);
                enviaIMC.putExtras(b);

                //Editor de dados do SharedPreferences
                Editor editor = lgShared.edit();

                //Gravar dados no SharedPreferences
                editor.putString("senha",senha);
                editor.commit();

                //quando não vai haver retorno de dados
                //startActivity(enviaIMC);

                //quando vai haver retorno de dados
                startActivityForResult(enviaIMC, 1);
            }else{
                Toast toast = Toast.makeText(this, "Senha incorreta!", Toast.LENGTH_LONG);
                toast.show();
            }

        }else{
            Toast toast = Toast.makeText(this, "Usuário não existe", Toast.LENGTH_LONG);
            toast.show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case  Activity.RESULT_CANCELED :
                return;
            case  COD_SEGUNDA_TELA :
                Bundle bundle = data.getExtras();
                String imc = bundle.getString("email");
                TextView resultado = (TextView) findViewById(R.id.email);
                resultado.setText(imc);
                Toast toast = Toast.makeText(this, "Lougout feito com sucesso!", Toast.LENGTH_LONG);
                toast.show();
                break;
        }
    }


    public void login(View view){
        verificaLogin();
    }
}
