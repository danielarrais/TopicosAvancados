package com.example.catolica2017.cadastroemtxt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorPessoa extends BaseAdapter {

    public ArrayList<Carro> carros;
    private Context context;

    public AdaptadorPessoa(ArrayList<Carro> pessoas, Context contexto) {
        this.carros = pessoas;
        this.context = contexto;
    }

    @Override
    public int getCount() {
        return carros.size();
    }

    @Override
    public Object getItem(int position) {
        return carros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View reciclado, ViewGroup parent) {
        View layout = null;
        if (reciclado==null){
            layout = LayoutInflater.from(context).inflate(R.layout.celula, parent, false);
        }else{
            layout = reciclado;
        }

        Carro carro = (Carro) getItem(position);

        TextView nome = layout.findViewById(R.id.lbNome);
        TextView sobreNome = layout.findViewById(R.id.lbSobrenome);
        TextView nascimento = layout.findViewById(R.id.lbNasc);

        nome.setText(carro.getNome());
        sobreNome.setText(carro.getId()+"");
        nascimento.setText(carro.getUrl_video()+"");

        return layout;
    }
}
